# Demo to setup gitlab CI for npm packages.

### Installing the Runner
1. Add GitLab’s official repository:
```
# For Debian/Ubuntu/Mint
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash

# For RHEL/CentOS/Fedora
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash
```
2. Install the latest version of GitLab Runner.
```
# For Debian/Ubuntu/Mint
sudo apt-get install gitlab-runner

# For RHEL/CentOS/Fedora
sudo yum install gitlab-runner
```


3. Register the Runner

```gitlab-runner register```
Runner token can be found under Settings -> CI / CD -> Runners

### Set up environment variables
* Project settings -> CI / CD -> Variables -> add your NPM_TOKEN enable Masked, but don't enable Protected State. -> Save variables

![alt text][logo]

[logo]: NPM_TOKEN.png "add NPM_TOKEN variable"

### GIT push

When upgrading the package version you will need to update the package.json version and then after you commit, 
*git tag (version)*. 
Then 
*git push --tags*